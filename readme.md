

*ec2-info*

O script adiciona informações da máquina no "motd" que exibido após o login


Last login: Sun Mar 25 14:44:49 2018 from 179.154.224.36  

       __|  __|_  )  
       _|  (     /   Amazon Linux AMI  
      ___|\___|___|  

https://aws.amazon.com/amazon-linux-ami/2017.09-release-notes/  
28 package(s) needed for security, out of 37 available  
Run "sudo yum update" to apply all updates.  
Amazon Linux version 2018.03 is available.  
  
   Type______: t2.nano  
   AZ________: us-east-1c  
   PrivIP____: 172.31.24.196  
   PubIP_____: 99.999.99.999  
   HostName__: ec2-99-999-99-999.compute-1.amazonaws.com  
  
  
Instalação

```bash
sudo wget https://gitlab.com/andersonesanto1/ec2info/-/archive/master/ec2info-master.zip
sudo chmod 755 ec2info-master/*
sudo ./ec2info-master/install.sh
```


